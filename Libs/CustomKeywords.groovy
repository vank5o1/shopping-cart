
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import com.kms.katalon.core.testobject.TestObject

import java.util.List

import org.openqa.selenium.WebElement

import com.applitools.eyes.selenium.Eyes

import com.applitools.eyes.RectangleSize



def static "sample.Login.loginIntoApplication"(
    	String applicationURL	
     , 	String username	
     , 	String password	) {
    (new sample.Login()).loginIntoApplication(
        	applicationURL
         , 	username
         , 	password)
}


def static "sample.Login.loginIntoApplicationWithGlobalVariable"() {
    (new sample.Login()).loginIntoApplicationWithGlobalVariable()
}


def static "sample.Login.logoutFromApplication"() {
    (new sample.Login()).logoutFromApplication()
}

 /**
	 * Select an option by label.
	 *
	 * @param to The select2 box object that tagged as "select" in the document.
	 * @param option The label of the option needs to select.
	 */ 
def static "sample.Select2.selectOptionByLabel"(
    	TestObject to	
     , 	String option	) {
    (new sample.Select2()).selectOptionByLabel(
        	to
         , 	option)
}

 /**
	 * Select many options by labels. This method just applies for multiple value select boxes.
	 *
	 * @param to The select2 box object that tagged as "select" in the document.
	 * @param options The labels of the options need to select.
	 */ 
def static "sample.Select2.selectManyOptionsByLabel"(
    	TestObject to	
     , 	java.util.List<String> options	) {
    (new sample.Select2()).selectManyOptionsByLabel(
        	to
         , 	options)
}

 /**
	 * Get labels of selected options.
	 *
	 * @param to The select2 box object that tagged as "select" in the document.
	 * @return A labels list of selected options.
	 */ 
def static "sample.Select2.getSelectedOptionsLabel"(
    	TestObject to	) {
    (new sample.Select2()).getSelectedOptionsLabel(
        	to)
}

 /**
	 * Get elements of selected options.
	 *
	 * @param to The select2 box object that tagged as "select" in the document.
	 * @return A list WebElement of selected options.
	 */ 
def static "sample.Select2.getSelectedOptionsList"(
    	TestObject to	) {
    (new sample.Select2()).getSelectedOptionsList(
        	to)
}

 /**
	 * Get labels of all options.
	 *
	 * @param to The select2 box object that tagged as "select" in the document.
	 * @return A list string of all options.
	 */ 
def static "sample.Select2.getAllOptionsLabel"(
    	TestObject to	) {
    (new sample.Select2()).getAllOptionsLabel(
        	to)
}

 /**
	 * Remove selected options. This method just applies for multiple value select boxes.
	 *
	 * @param to The select2 box object that tagged as "select" in the document.
	 * @param options The list of options needs to remove.
	 */ 
def static "sample.Select2.removeOptions"(
    	TestObject to	
     , 	java.util.List<String> options	) {
    (new sample.Select2()).removeOptions(
        	to
         , 	options)
}


def static "sample.Shop.navigatetoDetailPage"(
    	String productName	
     , 	String urlProduct	) {
    (new sample.Shop()).navigatetoDetailPage(
        	productName
         , 	urlProduct)
}


def static "sample.Shop.addToCart"(
    	String productName	
     , 	String urlProduct	) {
    (new sample.Shop()).addToCart(
        	productName
         , 	urlProduct)
}


def static "sample.Shop.applyCouponAndAddToCart"(
    	String productName	
     , 	String urlProduct	
     , 	String coupon	) {
    (new sample.Shop()).applyCouponAndAddToCart(
        	productName
         , 	urlProduct
         , 	coupon)
}


def static "sample.Shop.addToCartWithGlobalVariable"() {
    (new sample.Shop()).addToCartWithGlobalVariable()
}


def static "sample.Shop.applyCouponAndAddToCartWithGlobalVariable"() {
    (new sample.Shop()).applyCouponAndAddToCartWithGlobalVariable()
}


def static "sample.Checkout.CheckoutShop"(
    	String firstName	
     , 	String lastName	
     , 	String companyName	
     , 	String country	
     , 	String address	
     , 	String city	
     , 	String postCode	
     , 	String Phone	) {
    (new sample.Checkout()).CheckoutShop(
        	firstName
         , 	lastName
         , 	companyName
         , 	country
         , 	address
         , 	city
         , 	postCode
         , 	Phone)
}


def static "sample.Checkout.CheckoutShopWithGlobalVariable"() {
    (new sample.Checkout()).CheckoutShopWithGlobalVariable()
}

 /**
	 * Click on select2 text box.
	 *
	 * @param to The select2 box object that tagged as "select" in the document.
	 */ 
def static "sample.Utils.clickOnSelect2"(
    	TestObject to	) {
    (new sample.Utils()).clickOnSelect2(
        	to)
}

 /**
	 * Find the container class after the select element.
	 *
	 * @param to The select2 box object that tagged as "select" in the document.
	 * @return The WebElement that seen as container class after the select element.
	 */ 
def static "sample.Utils.findContainer"(
    	TestObject to	) {
    (new sample.Utils()).findContainer(
        	to)
}

 /**
	 * Select the result after filling text search to select2 text box.
	 *
	 * @param option The label of option need to select.
	 * @param subContainerOpenClass The Xpath string of the container class.
	 */ 
def static "sample.Utils.selectResult"(
    	String option	
     , 	String subContainerOpenClass	) {
    (new sample.Utils()).selectResult(
        	option
         , 	subContainerOpenClass)
}

 /**
	 * Enter text search to the select2 text box.
	 *
	 * @param option The label of option need to search.
	 * @return The Xpath string of the container class.
	 */ 
def static "sample.Utils.enterText"(
    	String option	) {
    (new sample.Utils()).enterText(
        	option)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.isVisible"(
    	WebElement element	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).isVisible(
        	element)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.scrollTo"(
    	WebElement element	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).scrollTo(
        	element)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.selectRadio"(
    	String label	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).selectRadio(
        	label)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.selectRadio"(
    	WebElement parentElement	
     , 	String label	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).selectRadio(
        	parentElement
         , 	label)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.isChecked"(
    	WebElement element	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).isChecked(
        	element)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.getParentElement"(
    	WebElement element	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).getParentElement(
        	element)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.isDisabled"(
    	WebElement element	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).isDisabled(
        	element)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.findElements"(
    	String cssSelector	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).findElements(
        	cssSelector)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.findElement"(
    	String cssSelector	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).findElement(
        	cssSelector)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.getPreviousSiblingElement"(
    	WebElement element	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).getPreviousSiblingElement(
        	element)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.findElementInsideParent"(
    	WebElement parent	
     , 	String cssSelector	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).findElementInsideParent(
        	parent
         , 	cssSelector)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.getAllShadowElement"(
    	WebElement parent	
     , 	String selector	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).getAllShadowElement(
        	parent
         , 	selector)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.getShadowElement"(
    	WebElement parent	
     , 	String selector	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).getShadowElement(
        	parent
         , 	selector)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.findElementsInsideParent"(
    	WebElement parent	
     , 	String cssSelector	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).findElementsInsideParent(
        	parent
         , 	cssSelector)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.getChildElements"(
    	WebElement parent	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).getChildElements(
        	parent)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.getSiblingElements"(
    	WebElement element	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).getSiblingElements(
        	element)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.getNextSiblingElement"(
    	WebElement element	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).getNextSiblingElement(
        	element)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.selectDropdown"(
    	WebElement parentElement	
     , 	String label	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).selectDropdown(
        	parentElement
         , 	label)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.selectDropdown"(
    	String label	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).selectDropdown(
        	label)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.selectCheckbox"(
    	String label	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).selectCheckbox(
        	label)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.selectCheckbox"(
    	WebElement parentElement	
     , 	String label	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).selectCheckbox(
        	parentElement
         , 	label)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.getSiblingElement"(
    	WebElement element	
     , 	String selector	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).getSiblingElement(
        	element
         , 	selector)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.setImplicitWait"(
    	int seconds	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).setImplicitWait(
        	seconds)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.setExplicitWait"(
    	int seconds	
     , 	int pollingTime	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).setExplicitWait(
        	seconds
         , 	pollingTime)
}


def static "com.katalon.plugin.keyword.shadow.ShadowKeywords.getAttribute"(
    	WebElement element	
     , 	String attribute	) {
    (new com.katalon.plugin.keyword.shadow.ShadowKeywords()).getAttribute(
        	element
         , 	attribute)
}


def static "com.kms.katalon.keyword.applitools.BasicKeywords.checkElement"(
    	Eyes eyes	
     , 	WebElement element	) {
    (new com.kms.katalon.keyword.applitools.BasicKeywords()).checkElement(
        	eyes
         , 	element)
}


def static "com.kms.katalon.keyword.applitools.BasicKeywords.checkTestObject"(
    	TestObject testObject	
     , 	String testName	) {
    (new com.kms.katalon.keyword.applitools.BasicKeywords()).checkTestObject(
        	testObject
         , 	testName)
}


def static "com.kms.katalon.keyword.applitools.BasicKeywords.checkWindow"(
    	String testName	) {
    (new com.kms.katalon.keyword.applitools.BasicKeywords()).checkWindow(
        	testName)
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesOpen"(
    	String testName	
     , 	RectangleSize viewportSize	) {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesOpen(
        	testName
         , 	viewportSize)
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesInit"() {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesInit()
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesClose"(
    	Eyes eyes	) {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesClose(
        	eyes)
}


def static "com.kms.katalon.keyword.applitools.EyesKeywords.eyesOpenWithBaseline"(
    	String baselineName	
     , 	String testName	
     , 	RectangleSize viewportSize	) {
    (new com.kms.katalon.keyword.applitools.EyesKeywords()).eyesOpenWithBaseline(
        	baselineName
         , 	testName
         , 	viewportSize)
}
